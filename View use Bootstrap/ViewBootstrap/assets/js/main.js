$(document).ready(function() {

    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
        $('.overlay').fadeIn();
    });




    $('.slide').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    $('.single-item').slick();

    $('.lazy').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1
    });

    $(".regular").slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });



    $('.portfolio-thumb-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.portfolio-item-slider',
        dots: false,
        focusOnSelect: true,
        infinite: false,
    });


    $('.portfolio-item-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 1500,
        infinite: true
    });



    $('.slider-feature').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        autoplaySpeed: 1500,
        infinite: true,
        centerMode: true,
        centerPadding: '10px'
    });


    var current = 0; // current slider dupa refresh
    $('.portfolio-thumb-slider .slick-slide:not(.slick-cloned)').eq(current).addClass('slick-current');
    $('.portfolio-item-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
        current = currentSlide;
        $('.portfolio-thumb-slider .slick-slide').removeClass('slick-current');
        $('.portfolio-thumb-slider .slick-slide:not(.slick-cloned)').eq(current).addClass('slick-current');
        var nrCurrentSlide = slick.currentSlide + 1, // din cauza ca e array si incepe de la 0
            totalSlidesPerPage = nrCurrentSlide + 3; // daca ai 5 thumb-uri pe pagina pui + 4

    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        draggable:false,
        autoplaySpeed: 2000,
        infinite: true,
        arrows: true,
        nextArrow: $('#up'),
        prevArrow: $('#down')
    });

    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 3,
        asNavFor: '.slider-for',
        dots: false,
        arrows: true,
        infinite: false,
        focusOnSelect: true
    });

});
